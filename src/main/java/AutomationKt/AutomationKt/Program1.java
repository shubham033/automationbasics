package AutomationKt.AutomationKt;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Program1 {
	WebDriver driver;

	public Program1() {
		System.setProperty("webdriver.chrome.driver",
				"/Users/ShubhamPatil/eclipse-workspace/Permormance/src/main/resources/chromedriver");
		driver = new ChromeDriver();
	}

	public static void main(String[] args) throws InterruptedException {
		Program1 program1 = new Program1();

		program1.hitUrl("Https://www.google.com");
		program1.validateWebpage("Google");
		program1.navigateToNewUrl("https://www.facebook.com/");
		program1.validateWebpage("Facebook – log in or sign up");
//		Thread.sleep(1000);
//		program1.navigateBack();
//		Thread.sleep(1000);
//		program1.navigateForward();

		program1.close();
	}

	public void hitUrl(String url) {
		driver.get(url);
		driver.manage().window().maximize();
	}

	public void validateWebpage(String expectedTitel) {
		System.out.println(driver.getTitle());

		if (driver.getTitle().equals(expectedTitel)) {
			System.out.println("Titles are matched");
		} else {
			System.out.println(
					"Title is not matched, Expected is :" + expectedTitel + " Actual is:- " + driver.getTitle());
		}

	}

	public void navigateToNewUrl(String url) throws InterruptedException {
		driver.navigate().to(url);
	}

	public void navigateBack() {
		driver.navigate().back();
	}

	public void navigateForward() {
		driver.navigate().forward();
	}

	public void close() {
		driver.close();
	}

}
